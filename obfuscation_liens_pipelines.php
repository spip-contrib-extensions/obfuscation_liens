<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function obfuscation_liens_insert_head($flux) {
	$flux .= "<script type='text/javascript' src='".find_in_path("obfuscation_liens.js")."'></script>";
	$flux .= "<style>[data-obf]{cursor:pointer;}</style>";
	return $flux;
}
