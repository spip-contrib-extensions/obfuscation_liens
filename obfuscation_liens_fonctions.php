<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


function _filtrer_obfuscation_liens ($reg) {
	$url = $reg[1];
	$url = base64_encode($url);
	$cont = $reg[2];
	
	return "<span data-obf=\"$url\"$cont</span>";
}

function obfuscation_liens ($texte) {
	$texte = preg_replace_callback(",<a href=[\'\"](.*)[\'\"](.*)</a>,Us", "_filtrer_obfuscation_liens", $texte );
	return $texte;
}
